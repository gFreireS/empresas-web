import React from 'react';
import { Switch, Route } from 'react-router-dom';
import LoginContainer from '@components/login-container';
import SearchContainer from '@components/search-company-container';

export default () => {
  return (
    <Switch>
      <Route exact path="/">
        <LoginContainer />
      </Route>
      <Route path="/empresas">
        <SearchContainer />
      </Route>
    </Switch>
  );
};

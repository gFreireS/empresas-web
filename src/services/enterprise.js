import api from './api';

const listEnterprises = async (name) => {
  const options = {
    method: 'get',
    url: `/enterprises?name=${name}`,
  };
  try {
    const response = await api(options);
    return response.data.enterprises;
  } catch (error) {
    throw new Error('Erro interno no servidor');
  }
};
export default listEnterprises;

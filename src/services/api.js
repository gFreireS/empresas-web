import axios from 'axios';
import { toast } from 'react-toastify';

const apiVersion = 'v1';

const accessToken = localStorage.getItem('accessToken');
const client = localStorage.getItem('client');
const uid = localStorage.getItem('uid');

const generateInstance = () => {
  if (accessToken) {
    return axios.create({
      baseURL: `https://empresas.ioasys.com.br/api/${apiVersion}/`,
      headers: {
        'access-token': accessToken,
        client,
        uid,
      },
    });
  }
  return axios.create({
    baseURL: `https://empresas.ioasys.com.br/api/${apiVersion}/`,
  });
};

const api = generateInstance();
api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401 && window.location.pathname !== '/') {
      toast.error('Autenticação expirada você será redirecionado para pagina de login');
      setTimeout(() => {
        window.location = '/';
      }, 4000);
    } else {
      return Promise.reject(error);
    }
  },
);

export default api;

import { get } from 'lodash';
import setHeaders from '@utils/setHeaders';
import api from './api';

const authUser = async (email, password) => {
  const options = {
    method: 'post',
    url: '/users/auth/sign_in',
    data: {
      email,
      password,
    },
  };
  try {
    const response = await api(options);
    localStorage.clear();
    setHeaders(response);
  } catch (error) {
    if (get(error, 'response.status') === 401) {
      throw new Error('Credenciais informadas são inválidas, tente novamente.');
    }
    throw new Error('Erro interno no Servidor');
  }
};
export default authUser;

import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
body {
  margin: 0;
  padding: 0;
  font-family: Roboto, Helvetica, Sans-Serif;
  background: #EEECDB;
}

@-webkit-keyframes spin {
  0% { transform: rotate(0deg); }

  100% { transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }

  100% { transform: rotate(360deg); }
}
`;

export default GlobalStyle;

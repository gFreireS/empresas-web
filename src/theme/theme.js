const theme = {
  background: '#eeecdb',
  mainColor: '#ee4c77',
  secondary: '#57bbbc',
  textMain: '#383743',
  textInput: '#403e4d',
  white: '#ffffff',
  darkIndigo: '#1a0e49',
  warning: '#ff0f44',
  gray: '#748383',
  warmGray: '#8d8c8c',
  pink: 'linear-gradient(180deg, #ee4c77 8%, #0d0430 400%)',
};
export default theme;

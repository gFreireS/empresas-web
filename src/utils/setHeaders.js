import { get } from 'lodash';
import api from '@services/api';

export default (response) => {
  const accessToken = get(response, 'headers.access-token');
  const client = get(response, 'headers.client');
  const uid = get(response, 'headers.uid');
  const commonHeaders = api.defaults.headers;
  commonHeaders['access-token'] = accessToken;
  commonHeaders.client = client;
  commonHeaders.uid = uid;
  const storage = localStorage;
  storage.setItem('accessToken', accessToken);
  storage.setItem('client', client);
  storage.setItem('uid', uid);
};

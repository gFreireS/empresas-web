const generateImageString = (imageName) => {
  const imagesSrc = `/src/assets/images/${imageName}/`;
  const basePath = `${imagesSrc}${imageName}`;
  return {
    srcSet: `${basePath}@2x.png 2x, ${basePath}@3x.png 3x`,
    src: `${basePath}.png`,
  };
};
export default generateImageString;

import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import LoginContainer from './LoginContainer';

const testUrl = 'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in';

const server = setupServer(
  rest.post(testUrl, (req, res, ctx) => {
    return res(
      ctx.json({
        test: 'test string',
      }),
      ctx.set('access-token', 'aaaa'),
      ctx.set('client', 'aaaa'),
      ctx.set('uid', 'aaaa'),
    );
  }),
);

describe('Login Container', () => {
  test('Render Login Container', () => {
    const component = render(<LoginContainer />);
    expect(component).toMatchSnapshot();
  });

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  test('Should make the password visible then return to hidden', () => {
    const component = render(<LoginContainer />);
    const passwordInput = component.getByTestId('password-input');
    const visibilityIcon = component.getByTestId('visibility-icon');
    fireEvent.click(visibilityIcon);
    expect(passwordInput.type).toBe('text');
    fireEvent.click(visibilityIcon);
    expect(passwordInput.type).toBe('password');
  });

  test('Should login, get credentials and change page', async () => {
    const history = createMemoryHistory();
    const spy = jest.spyOn(history, 'push');
    const component = render(
      <Router history={history}>
        <LoginContainer />
      </Router>,
    );
    const passwordInput = component.getByTestId('password-input');
    const emailInput = component.getByTestId('email-input');
    const button = component.getByTestId('login-button');
    fireEvent.change(emailInput, { target: { value: 'aaa' } });
    fireEvent.change(passwordInput, { target: { value: 'aaa' } });
    fireEvent.click(button);
    await waitFor(() => expect(spy).toBeCalledWith('/empresas'));
  });

  test('Should show an error about password', () => {
    const history = createMemoryHistory();
    const component = render(
      <Router history={history}>
        <LoginContainer />
      </Router>,
    );
    const emailInput = component.getByTestId('email-input');
    const button = component.getByTestId('login-button');
    fireEvent.change(emailInput, { target: { value: 'aaa' } });
    fireEvent.click(button);
    const error = component.getByText('Os seguintes campos estão vazios: Senha');
    expect(error).toBeTruthy();
  });

  test('Should show an error about email', () => {
    const history = createMemoryHistory();
    const component = render(
      <Router history={history}>
        <LoginContainer />
      </Router>,
    );
    const passwordInput = component.getByTestId('password-input');
    const button = component.getByTestId('login-button');
    fireEvent.change(passwordInput, { target: { value: 'aaa' } });
    fireEvent.click(button);
    const error = component.getByText('Os seguintes campos estão vazios: E-mail');
    expect(error).toBeTruthy();
  });

  test('Should show an error about credentials', async () => {
    server.use(
      rest.post(testUrl, (req, res, ctx) => {
        return res(
          ctx.status(401),
          ctx.json({
            test: 'test string',
          }),
        );
      }),
    );
    const history = createMemoryHistory();
    const component = render(
      <Router history={history}>
        <LoginContainer />
      </Router>,
    );
    const passwordInput = component.getByTestId('password-input');
    const emailInput = component.getByTestId('email-input');
    const button = component.getByTestId('login-button');
    fireEvent.change(emailInput, { target: { value: 'aaa' } });
    fireEvent.change(passwordInput, { target: { value: 'aaa' } });
    await fireEvent.click(button);
    const loader = component.getByTestId('loader');
    await waitFor(() => expect(loader).not.toBeInTheDocument());
    const error = component.getByText('Credenciais informadas são inválidas, tente novamente.');
    expect(error).toBeTruthy();
  });
});

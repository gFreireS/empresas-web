import React from 'react';
import styled from 'styled-components';

const baseLoaderSize = '12px';
const Loader = styled.div`
  position: absolute;
  bottom: 43%;
  width: 120px;
  height: 120px;
  border: ${baseLoaderSize} solid transparent;
  border-top: ${baseLoaderSize} solid ${(props) => props.theme.secondary};
  border-right: ${baseLoaderSize} solid ${(props) => props.theme.secondary};
  border-bottom: ${baseLoaderSize} solid ${(props) => props.theme.secondary};
  border-radius: 50%;
  animation: spin 1s linear infinite;
  animation: spin 1s linear infinite;
`;
const LoaderWrapper = styled.div`
  position: absolute;
  top: 0;
  z-index: 999;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100vh;
  overflow: hidden;
  background-color: rgba(255, 255, 255, 0.6);
  backdrop-filter: blur(1px);
`;

const LoginLoader = ({ ...props }) => {
  return (
    <LoaderWrapper {...props}>
      <Loader />
    </LoaderWrapper>
  );
};

export default LoginLoader;

import React, { useState } from 'react';
import styled from 'styled-components';
import Img from '@components/image';
import LoginForm from '@components/login-container/login-form';
import Loader from '@components/login-container/login-loader';
import signIn from '@services/user';
import { useHistory } from 'react-router-dom';

const LoginWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  height: 100vh;
`;
const LoginComponent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 32rem;
  height: 32rem;
  margin: 0 auto;
`;
const LoginHeader = styled.h1`
  width: 11rem;
  height: 4rem;
  margin-top: 4.118rem;
  color: ${(props) => props.theme.textMain};
  font-weight: bold;
  font-size: 1.5rem;
  font-family: Roboto;
  text-align: center;
`;
const LoginSubHeader = styled.h2`
  width: 22.359rem;
  height: 3.25rem;
  margin: 0;
  padding: 0;
  color: ${(props) => props.theme.textMain};
  font-weight: normal;
  font-size: 1.125rem;
  font-family: Roboto;
  line-height: 1.44;
  letter-spacing: -0.13px;
  text-align: center;
`;

const LoginContainer = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [formError, setFormError] = useState();
  const history = useHistory();

  const onSubmit = async (e) => {
    e.preventDefault();
    const currentForm = e.target;
    const email = currentForm.elements[0];
    const password = currentForm.elements[1];
    setIsLoading(true);
    try {
      await signIn(email.value, password.value);
      history.push('/empresas');
      return;
    } catch (error) {
      setIsLoading(false);
      setFormError(error);
    }
    setIsLoading(false);
  };
  return (
    <LoginWrapper>
      {isLoading && <Loader data-testid="loader" />}
      <LoginComponent>
        <Img srcName="logo-home" />
        <LoginHeader>BEM-VINDO AO EMPRESAS</LoginHeader>
        <LoginSubHeader>
          Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
        </LoginSubHeader>
        <LoginForm onSubmit={onSubmit} loginError={formError} setFormError={setFormError} />
      </LoginComponent>
    </LoginWrapper>
  );
};

export default LoginContainer;

import React from 'react';
import styled from 'styled-components';

const Button = styled.input`
  width: ${(props) => (props.fullWidth ? "100%" : "20.063rem")};
  height: 3.563rem;
  color: ${(props) => props.theme.white};
  font-weight: 600;
  font-size: 1.208rem;
  font-family: GillSans;
  text-align: center;
  background-color: ${(props) => props.theme.secondary};
  border: none;
  border-radius: 3.9px;
  cursor: pointer;
  appearance: none;

  &:focus {
    outline: none;
  }

  &:disabled {
    background-color: ${(props) => props.theme.gray};
    opacity: 56%;
  }
`;
const LoginButton = ({ error, fullWidth, ...props }) => {
  return <Button fullWidth={fullWidth} disabled={error} {...props} />;
};

export default LoginButton;

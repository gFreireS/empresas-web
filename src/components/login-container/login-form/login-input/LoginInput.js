import React from 'react';
import styled from 'styled-components';
import { ErrorIcon } from '@components/icons';

const Input = styled.input`
  width: 21.75rem;
  padding: 0.25rem 2.313rem;
  color: ${(props) => props.theme.textInput};
  font-weight: thin;
  font-size: 1.36rem;
  font-family: Roboto;
  letter-spacing: -0.31px;
  background: none;
  border: none;
  border-bottom: 0.6px solid ${(props) => (props.error ? props.theme.mainColor : props.theme.gray)};

  &:placeholder {
    color: ${(props) => props.theme.textMain};
    font-weight: 300;
    font-size: 1.125rem;
    letter-spacing: -0.25px;
    text-align: left;
    opacity: 50%;
  }

  &::-webkit-input-placeholder {
    font-weight: 300;
    opacity: 50%;
  }

  &:focus {
    outline: none;
  }

  &:-webkit-autofill::first-line,
  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus,
  &:-webkit-autofill:active {
    color: ${(props) => props.theme.textInput};
    font-size: 1.369rem;
    box-shadow: 0 0 0 30px ${(props) => props.theme.background} inset;
  }
`;

const LeftIcon = styled.span`
  position: absolute;
  top: 3px;
  align-items: center;
  width: 20px;
  height: 20px;
`;
const RightIcon = styled.span`
  position: absolute;
  top: 5px;
  right: 0;
  display: flex;
  align-items: center;

  svg {
    position: relative;
    bottom: 5px;
    width: 35px;
    height: 35px;
    pointer-events: none;
  }
`;

const InputWrapper = styled.div`
  position: relative;
  display: flex;
  box-sizing: border-box;
  max-width: 21.75rem;
  margin-top: 2.063rem;

  &:first-child {
    margin-top: 2.875rem;
    font-size: 1.36rem;
  }

  &:last-child {
    margin-bottom: 0;

    * {
      font-family: LucidaGrande;
    }
  }
`;

const LoginInput = ({ missing, name, error, rightIcon, leftIcon, ...props }) => {
  const isErrorInput = () => {
    if (missing) {
      const isMissing = !!missing.find((field) => field.name === name);
      return isMissing;
    }
    return error;
  };
  return (
    <InputWrapper>
      {leftIcon && <LeftIcon>{leftIcon}</LeftIcon>}
      <Input error={isErrorInput()} name={name} {...props} />
      <RightIcon>{isErrorInput() ? <ErrorIcon /> : rightIcon}</RightIcon>
    </InputWrapper>
  );
};

export default LoginInput;

import React, { useState, useRef } from 'react';
import styled from 'styled-components';
import LoginInput from '@components/login-container/login-form/login-input';
import { EmailIcon, PasswordIcon, VisibilityIcon } from '@components/icons';
import LoginButton from '@components/login-container/login-form/login-button';

const CredendialsForm = styled.form`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 21.938rem;
`;
const LoginError = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 3rem;
  color: ${(props) => props.theme.warning};
  font-size: 0.76rem;
  font-family: Roboto;
  line-height: 1.95;
  letter-spacing: -0.17px;
  text-align: center;
`;

const LoginForm = ({ onSubmit, loginError, setFormError }) => {
  const formEl = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const [isFullWidth, setIsFullWidth] = useState(false);
  const [missingFields, setMissingFields] = useState(false);
  const [formValue, setFormValue] = useState({
    email: {
      name: 'email',
      text: 'E-mail',
      value: '',
    },
    password: {
      name: 'password',
      text: 'Senha',
      value: '',
    },
  });
  const handleButtonSize = () => {
    const fields = Object.values(formValue);
    const notFilled = fields.filter((field) => field.value.length < 1);
    if (!notFilled.length) {
      setIsFullWidth(true);
    } else {
      setIsFullWidth(false);
    }
  };

  const handleInput = (e) => {
    const currentName = e.target.name;
    const currentValues = formValue;
    currentValues[e.target.name].value = e.target.value;
    setFormValue(currentValues);
    handleButtonSize();
    if (loginError) {
      if (missingFields) {
        const wasMissing = missingFields.find((field) => field.name === currentName);
        setMissingFields(wasMissing && null);
      }
      setFormError(false);
    }
  };
  const handleFormSubmit = (e) => {
    const fields = Object.values(formValue);
    const notFilled = fields.filter((field) => field.value.length < 1);
    const fieldNames = notFilled.map((field) => ` ${field.text}`);
    if (notFilled.length) {
      e.preventDefault();
      setMissingFields(notFilled);
      setFormError(new Error(`Os seguintes campos estão vazios:${fieldNames}`));
      return;
    }
    setMissingFields(null);
    onSubmit(e);
  };

  return (
    <CredendialsForm onSubmit={handleFormSubmit} ref={formEl}>
      <LoginInput
        missing={missingFields}
        placeholder="E-mail"
        name="email"
        error={loginError}
        data-testid="email-input"
        leftIcon={<EmailIcon />}
        onChange={handleInput}
      />
      <LoginInput
        name="password"
        missing={missingFields}
        placeholder="Senha"
        onChange={handleInput}
        error={loginError}
        data-testid="password-input"
        type={isVisible ? 'text' : 'password'}
        leftIcon={<PasswordIcon />}
        rightIcon={(
          <VisibilityIcon
            data-testid="visibility-icon"
            filled={isVisible}
            onClick={() => setIsVisible(!isVisible)}
          />
        )}
      />
      <LoginError>{loginError && loginError.message}</LoginError>
      <LoginButton
        data-testid="login-button"
        fullWidth={!loginError && isFullWidth}
        type="submit"
        value="ENTRAR"
        error={loginError}
      />
    </CredendialsForm>
  );
};

export default LoginForm;

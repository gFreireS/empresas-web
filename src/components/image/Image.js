import React from 'react';
import imgSrcs from '@utils/generate-image-srcs';

const Img = ({ srcName, ...props }) => {
  return <img {...imgSrcs(srcName)} {...props} alt={srcName} />;
};

export default Img;

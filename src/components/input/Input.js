import React from 'react';
import styled from 'styled-components';
import { ErrorIcon } from '@components/icons';

const Input = styled.input`
  padding: 0.25rem 2.313rem;
  color: ${(props) => props.theme.textInput};
  font-weight: thin;
  font-size: 1.36rem;
  font-family: Roboto;
  letter-spacing: -0.31px;
  background: none;
  border: none;
  border-bottom: 0.6px solid ${(props) => (props.error ? props.theme.mainColor : props.theme.gray)};

  &:placeholder {
    color: ${(props) => props.theme.textMain};
    font-weight: 300;
    font-size: 1.125rem;
    letter-spacing: -0.25px;
    text-align: left;
    opacity: 50%;
  }

  &::-webkit-input-placeholder {
    font-weight: 300;
    opacity: 50%;
  }

  &:focus {
    outline: none;
  }

  &:-webkit-autofill::first-line,
  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus,
  &:-webkit-autofill:active {
    color: ${(props) => props.theme.textInput};
    font-size: 1.369rem;
    box-shadow: 0 0 0 30px ${(props) => props.theme.background} inset;
  }
`;

const LeftIcon = styled.span`
  position: absolute;
  top: 3px;
  align-items: center;
  width: 30px;
  height: 30px;

  svg {
    position: relative;
    bottom: 5px;
    width: 35px;
    height: 35px;
    pointer-events: none;
  }
`;
const RightIcon = styled.span`
  position: absolute;
  top: 5px;
  right: 0;
  display: flex;
  align-items: center;

  svg {
    position: relative;
    bottom: 5px;
    width: 35px;
    height: 35px;
    pointer-events: none;
  }
`;

const InputWrapper = styled.div`
  display: flex;
  box-sizing: border-box;
  margin-top: 2.063rem;
`;

const CustomInput = ({ rightIcon, onClick, leftIcon, className, ...props }) => {
  return (
    <InputWrapper className={className} onClick={onClick}>
      <LeftIcon>{leftIcon}</LeftIcon>
      <Input {...props} />
      <RightIcon>{rightIcon}</RightIcon>
    </InputWrapper>
  );
};

export default CustomInput;

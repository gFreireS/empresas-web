import React from 'react';

const CloseIcon = ({ ...props }) => {
  return (
    <div {...props}>
      <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
        <g fill="none" fillRule="evenodd" stroke="#FFF" strokeLinecap="square" strokeWidth="2.25">
          <path d="M22.293 22.293L38.73 38.73M38.707 22.293L22.27 38.73" />
        </g>
      </svg>
    </div>
  );
};

export default CloseIcon;

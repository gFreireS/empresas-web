import React from 'react';
import styled from 'styled-components';

const ErrorOval = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 1.438rem;
  height: 1.5rem;
  color: ${(props) => props.theme.white};
  font-family: Poppins;
  font-size: 1rem;
  background: ${(props) => props.theme.warning};
  border-radius: 50%;
`;
const ErrorIcon = () => {
  return <ErrorOval>!</ErrorOval>;
};

export default ErrorIcon;

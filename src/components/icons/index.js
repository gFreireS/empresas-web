import EmailIcon from './EmailIcon';
import PasswordIcon from './PasswordIcon';
import VisibilityIcon from './VisibilityIcon';
import ErrorIcon from './ErrorIcon';
import SearchIcon from './SearchIcon';
import BackIcon from './BackIcon';
import CloseIcon from './CloseIcon';

export { EmailIcon, PasswordIcon, VisibilityIcon, ErrorIcon, SearchIcon, BackIcon, CloseIcon };

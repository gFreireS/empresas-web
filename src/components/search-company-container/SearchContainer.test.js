import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import SearchContainer from './SearchContainer';

const testUrl = 'https://empresas.ioasys.com.br/api/v1/enterprises';

const server = setupServer(
  rest.get(testUrl, (req, res, ctx) => {
    return res(
      ctx.json({
        enterprises: [
          {
            photo: 'asdasdsad1',
            enterprise_name: 'hajskhd',
            enterprise_type: {
              enterprise_type_name: 'asdasda',
            },
            description: 'ashjdkhaskjdhjkahsjkdhjkasdjk',
            country: 'br',
          },
          {
            photo: 'asdasdsad2',
            enterprise_name: 'hajskhd',
            enterprise_type: {
              enterprise_type_name: 'asdasda',
            },
            description: 'ashjdkhaskjdhjkahsjkdhjkasdjk',
            country: 'br',
          },
          {
            photo: 'asdasd',
            enterprise_name: 'click-me',
            enterprise_type: {
              enterprise_type_name: 'asdasda',
            },
            description: 'description',
            country: 'br',
          },
        ],
      }),
    );
  }),
);

describe('Search Container', () => {
  test('Should Render the search container', () => {
    const component = render(<SearchContainer />);
    expect(component).toMatchSnapshot();
  });

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  test('Should search and show enterprises', async () => {
    const component = render(<SearchContainer />);
    const input = component.getByTestId('search');
    fireEvent.change(input, { target: { value: 'aaa' } });
    await waitFor(() => component.getByText('click-me'), {
      timeout: 5000,
    });
  });

  test(`Should click on an enteprise and view it's description`, async () => {
    const component = render(<SearchContainer />);
    const input = component.getByTestId('search');
    fireEvent.change(input, { target: { value: 'aaa' } });
    await waitFor(() => component.getByText('click-me'), {
      timeout: 5000,
    });
    const click = component.getByText('click-me');
    fireEvent.click(click);
    expect(component.getByText('description')).toBeInTheDocument();
  });
});

import React, { useState, useCallback } from 'react';
import { debounce } from 'lodash';
import styled from 'styled-components';
import listEnterprises from '@services/enterprise';
import Img from '@components/image';
import Input from '@components/input';
import EnterpriseList from '@components/search-company-container/search-list';
import NavBar from '@components/search-company-container/search-navbar';
import EnterpriseModal from '@components/search-company-container/search-modal';
import { SearchIcon, CloseIcon } from '@components/icons';

const Image = styled(Img)`
  height: 40px;
  opacity: ${(props) => (props.visible ? '100%' : '0%')};
  transition: all 0.5s ease-in-out;
`;

const EnterpriseWrapper = styled.div`
  position: relative;
  display: block;
  width: 100%;
  height: 100vh;
`;

const SearchInput = styled(Input)`
  position: absolute;
  right: ${(props) => (props.visible ? 'initial' : '40px')};
  width: ${(props) => (props.visible ? '95%' : '2%')};
  margin-top: 10px;
  color: white;
  transition: all 0.3s ease-in;

  input {
    width: 100%;
    padding-left: 50px;
    color: white;
    border-bottom: 1px solid ${(props) => (props.visible ? props.theme.white : 'transparent')};
    border-radius: 0;
    transition: width 0.3s linear;
    appearance: none;
    pointer-events: ${(props) => (props.visible ? 'all' : 'none')};
  }

  svg {
    bottom: 15px;
    width: 50px;
    height: 50px;
  }
`;

const SearchContainer = () => {
  const [isInputVisible, setIsInputVisible] = useState(false);
  const [enterpriseList, setEnterpriseList] = useState(null);
  const [currentEnterprise, setCurrentEnterprise] = useState({});
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [disabledInput, setDisableInput] = useState(false);
  const baseUrl = 'https://empresas.ioasys.com.br';

  const debouncedSearch = useCallback(
    debounce(async (e) => {
      const enterprises = await listEnterprises(e.target.value);
      setEnterpriseList(enterprises);
    }, 1000),
    [],
  );

  const inputChange = (e) => {
    setInputValue(e.target.value);
    e.persist();
    debouncedSearch(e);
  };

  const clearList = (e) => {
    setInputValue('');
    setIsInputVisible(false);
    setEnterpriseList(null);
    e.target.value = '';
  };

  const showModal = (enterprise) => {
    document.body.style.overflow = 'hidden';
    setCurrentEnterprise(enterprise);
    setIsModalVisible(true);
  };

  return (
    <EnterpriseWrapper>
      <EnterpriseModal
        isModalVisible={isModalVisible}
        currentEnterprise={currentEnterprise}
        setIsModalVisible={setIsModalVisible}
        baseUrl={baseUrl}
      />
      <NavBar>
        <Image srcName="logo-nav" visible={!isInputVisible} />
        <SearchInput
          disabled={disabledInput}
          value={inputValue}
          onChange={inputChange}
          data-testid="search"
          visible={isInputVisible}
          leftIcon={<SearchIcon onClick={() => setIsInputVisible(true)} />}
          rightIcon={isInputVisible && <CloseIcon onClick={clearList} />}
        />
      </NavBar>
      <EnterpriseList enterpriseList={enterpriseList} showModal={showModal} baseUrl={baseUrl} />
    </EnterpriseWrapper>
  );
};

export default SearchContainer;

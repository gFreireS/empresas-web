import React from 'react';
import styled from 'styled-components';

const NavBar = styled.nav`
  position: sticky;
  top: 0;
  z-index: 3;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 85px;
  overflow: hidden;
  color: white;
  font-family: Roboto;
  background-image: ${(props) => props.theme.pink};
`;

const searchNavbar = ({ children }) => {
  return <NavBar>{children}</NavBar>;
};

export default searchNavbar;

import React from 'react';
import NavBar from '@components/search-company-container/search-navbar';
import styled from 'styled-components';
import { BackIcon } from '@components/icons';

const Enterprisemodal = styled.div`
  position: fixed;
  top: 0;
  left: ${(props) => (props.visible ? '0' : '100%')};
  z-index: 999;
  box-sizing: border-box;
  width: 100vw;
  height: 100%;
  overflow: auto;
  font-size: 1.5rem;
  font-family: Roboto;
  background-color: ${(props) => props.theme.background};
  transition: all 0.2s ease-in;

  &:-webkit-scrollbar {
    width: 7px;
    appearance: none;
  }

  &:-webkit-scrollbar-thumb {
    background-color: rgba(0, 0, 0, 0.5);
    border-radius: 4px;
    box-shadow: 0 0 1px rgba(255, 255, 255, 0.5);
  }
`;

const EnterpriseImageInfo = styled.img`
  width: 100%;
  height: 18.438rem;
  margin-bottom: 3rem;
  object-fit: cover;
`;

const ListContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  box-sizing: border-box;
  width: 100%;
  height: 85%;
`;

const StyledBackIcon = styled(BackIcon)`
  display: contents;
  cursor: pointer;

  svg {
    position: absolute;
    left: 20px;
    width: 40px;
    height: 40px;
  }
`;

const DescriptionBox = styled.div`
  flex-grow: 2;
  box-sizing: border-box;
  width: 95%;
  width: 95%;
  margin-top: 50px;
  margin-bottom: 40px;
  padding: 3rem 4.563rem;
  padding-bottom: 40px;
  color: ${(props) => props.theme.warmGray};
  font-size: 1.5rem;
  font-family: Roboto;
  background-color: white;
  border-radius: 4.8px;
  @media only screen and (max-width: 600px) {
    margin-top: 10px;
    padding: 10px;
  }

  p {
    margin: 0;
  }
`;

const SearchModal = ({ isModalVisible, currentEnterprise, setIsModalVisible, baseUrl }) => {
  return (
    <Enterprisemodal visible={isModalVisible}>
      {currentEnterprise.enterprise_name && (
        <>
          <NavBar>
            <StyledBackIcon
              onClick={() => {
                setIsModalVisible(false);
                document.body.style.overflow = 'initial';
              }}
            />
            {currentEnterprise.enterprise_name}
          </NavBar>
          <ListContainer>
            <DescriptionBox>
              <EnterpriseImageInfo src={baseUrl + currentEnterprise.photo} />
              <p>{currentEnterprise.description}</p>
            </DescriptionBox>
          </ListContainer>
        </>
      )}
    </Enterprisemodal>
  );
};

export default SearchModal;

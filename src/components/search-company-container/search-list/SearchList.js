import React from 'react';
import styled from 'styled-components';

const ListContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  box-sizing: border-box;
  width: 100%;
  height: 85%;
`;

const EnterpriseImage = styled.img`
  width: 18.313rem;
  object-fit: contain;
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;

const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  width: 100%;
  height: 85%;
`;

const EnterpriseInfo = styled.div`
  margin-left: 2.461rem;
  @media only screen and (max-width: 600px) {
    margin-left: 0;
  }
`;

const EnterpriseName = styled.h3`
  margin: 0;
  margin-top: 1.438rem;
  color: ${(props) => props.theme.darkIndigo};
  font-weight: bold;
  font-size: 1.5rem;
  font-family: Roboto;
  text-align: left;
`;

const EnterpriseType = styled.p`
  margin: 0;
  padding: 0.5rem 0;
  color: ${(props) => props.theme.warmGray};
  font-size: 1.5rem;
  font-family: Roboto;
  text-align: left;
`;

const EnterpriseCountry = styled.p`
  margin: 0;
  color: ${(props) => props.theme.warmGray};
  font-size: 1.125rem;
  font-family: Roboto;
  text-align: left;
`;

const EnterpriseCard = styled.div`
  display: flex;
  box-sizing: border-box;
  width: 95%;
  margin-top: 2.75rem;
  padding: 1.688rem 1.914rem;
  background-color: ${(props) => props.theme.white};
  border-radius: 4.7px;
  cursor: pointer;
  transition: all 0.13s ease-in-out;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
    flex-shrink: 0;
  }

  &:hover {
    box-shadow: 2px 4px 11px 2px rgba(0, 0, 0, 0.08);
  }

  &:last-child {
    margin-bottom: 2.75rem;
  }
`;

const SearchList = ({ enterpriseList, showModal, baseUrl }) => {
  return (
    <ListContainer>
      {enterpriseList && enterpriseList.length ? (
        enterpriseList.map((enterprise) => (
          <EnterpriseCard
            key={enterprise.id}
            className="animate__animated animate__fadeIn"
            onClick={() => showModal(enterprise)}
          >
            <EnterpriseImage src={baseUrl + enterprise.photo} />
            <EnterpriseInfo>
              <EnterpriseName>{enterprise.enterprise_name}</EnterpriseName>
              <EnterpriseType>{enterprise.enterprise_type.enterprise_type_name}</EnterpriseType>
              <EnterpriseCountry>{enterprise.country}</EnterpriseCountry>
            </EnterpriseInfo>
          </EnterpriseCard>
        ))
      ) : (
        <InfoContainer>
          {enterpriseList ? (
            <p>Nenhuma empresa foi encontrada para a busca realizada.</p>
          ) : (
            <p>Clique na busca para iniciar.</p>
          )}
        </InfoContainer>
      )}
    </ListContainer>
  );
};

export default SearchList;

import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'styled-components';
import theme from '@theme';
import { BrowserRouter as Router } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import GlobalStyle from './theme/globalStyle';
import Routes from './routes/routesConfig';
import 'react-toastify/dist/ReactToastify.css';

const wrapper = document.getElementById('container');

const Container = () => {
  return (
    <>
      <ToastContainer closeOnClick={false} pauseOnHover={false} autoClose={4000} />
      <Router>
        <GlobalStyle />
        <ThemeProvider theme={theme}>
          <Routes />
        </ThemeProvider>
      </Router>
    </>
  );
};
ReactDOM.render(<Container />, wrapper);

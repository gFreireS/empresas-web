# EMPRESAS WEB #

Projeto para teste an empresa Ioasys

## Comandos ##

* Instalação de dependências: `npm install`
* Iniciar o Projeto: `npm start`
* Gerar build do projeto: `npm run build`
* Executar testes unitários: `npm test`

### Stack Utilizada ###

* React
* Styled Components
* Webpack
* Axios
* Jest
* React testing library





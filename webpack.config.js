const HtmlWebPackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  resolve: {
    alias: {
      '@components': path.resolve(__dirname, 'src', 'components'),
      '@images': path.resolve(__dirname, 'src', 'assets', 'images'),
      '@utils': path.resolve(__dirname, 'src', 'utils'),
      '@theme': path.resolve(__dirname, 'src', 'theme', 'theme.js'),
      '@services': path.resolve(__dirname, 'src', 'services'),
    },
  },
  devServer: {
    historyApiFallback: true,
    host: '0.0.0.0',
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['file-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html',
    }),
  ],
};
